'use strict';

$('.left_block .btn').on('click', function () {
    $('.pop_up').addClass('active');
    $('.pop_up').removeClass('disactive');
    $('.wrapper').addClass('blur');
    $('.closePop').css('display', 'block');
});

$('.pop_up .closes').on('click', function () {
    $('.pop_up').removeClass('active');
    $('.pop_up').addClass('disactive');
    $('.wrapper').removeClass('blur');
    $('.closePop').css('display', 'none');
});

$('.closePop').on('click', function () {
    $('.pop_up').removeClass('active');
    $('.pop_up').addClass('disactive');
    $('.wrapper').removeClass('blur');
    $('.closePop').css('display', 'none');
});

$(document).ready(function () {
    $(".modal-form__file input[type=file]").change(function () {
        var filename = $(this).val().replace(/.*\\/, "");
        var sliced = filename.slice(0, 19);
        if (sliced.length < filename.length) {
            sliced += '...';
        }
        $("#filename").text(sliced);
        $(".upload-photo").css("display", "flex");
        $("#upload-photo-none").css("display", "none");
    });
});

$(".pop_up form").submit(function (e) {
    e.preventDefault();
    var ths = $(this);
    var serialize = new FormData(this);
    $.ajax({
        type: "POST",
        url: "/ajax/form2.php",
        data: serialize,
        dataType: "json",
        processData: false,
        contentType: false,
        success: function success(data) {
            ths.find("*").removeClass("error__form");
            data.forEach(function (item) {
                if (item == "success") {
                    $(".modal-form__complete").html("<h2>Ваше сообщение отправлено!</h2><p>Мы свяжемся с вами в билайжшее время!</p>");
                    $(".modal-form").remove();
                    $(".modal-form__complete").css("display", "flex");
                } else if (item == "checkbox") {
                    ths.find("label").addClass("error__form_checkbox");
                } else {
                    ths.find("[name='" + item + "']").addClass("error__form");
                }
            });
        }
    });
});
//# sourceMappingURL=script.js.map
